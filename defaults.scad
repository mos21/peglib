// NOSTL
inch = 25.4;
base_size = inch;
wall_thickness = inch / 8;
default_back_wall_gap = 0.2;
default_pegboard_chamfer = wall_thickness / 4;

